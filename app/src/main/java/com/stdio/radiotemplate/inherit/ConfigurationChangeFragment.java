package com.stdio.radiotemplate.inherit;

/**
 * This is an interface for fragments to indicate that it will handle configuration changes itself
 */
public interface ConfigurationChangeFragment {

}
